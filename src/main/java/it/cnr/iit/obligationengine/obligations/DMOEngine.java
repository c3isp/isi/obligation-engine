/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.obligationengine.obligations;
import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import it.cnr.iit.common.types.DataManipulationObject;
import it.cnr.iit.isi.api.restapi.types.PrivacyNetRequest;
import it.cnr.iit.isi.api.restapi.types.PrivacyNetResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.logging.Level;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

/**
 *
 * @author Vincenzo Farruggia
 * TODO: DMOToolBox
 */
@Component
public class DMOEngine {
    	private static final Logger LOG = Logger.getLogger(DMOEngine.class.getName());

	@Autowired
	private RestTemplate restTemplate;
        
        public String executeDMO(DataManipulationObject dmo) {
            LOG.info("Execute dmo: " + dmo.getName());

            switch (dmo.getName()) {
            case "AnonymiseIPv4ByRandomization":
            case "NotifyUserOn":
                // for old compatibility
                return DMOExecution.executeDMO(dmo);
            case "AnonymizationWithPrivacyNet":
                return runAnonymizationWithPrivacyNet(dmo);
            case "AnonymizeForSparta":
                return runAnonymizationSparta(dmo, dmo.getOptions());
//            case "AnonymizeForSparta-IpSrc":
//                return runAnonymizationSparta(dmo, "ip-src");
//            case "AnonymizeForSparta-IpDst":
//                return runAnonymizationSparta(dmo, "ip-dst");
//            case "AnonymizeForSparta-HostSrcPort":
//                return runAnonymizationSparta(dmo, "host_src_port");
//            case "AnonymizeForSparta-HostDstPort":
//                return runAnonymizationSparta(dmo, "host_dst_port");
//            case "AnonymizeForSparta-IdsEngineName":
//                return runAnonymizationSparta(dmo, "ids_engine_name");
//            case "AnonymizeForSparta-IdsEngineIp":
//                return runAnonymizationSparta(dmo, "ids_engine_ip");
//            case "AnonymizeForSparta-IdsEensorType":
//                return runAnonymizationSparta(dmo, "ids_sensor_type");
                
            default:
                LOG.severe("unknown dmo to execute");
            }
            return null;
	}
        
        private String runAnonymizationWithPrivacyNet(DataManipulationObject dmo) {
            String content = dmo.getAdditionalInfo().get("content");
            if (content == null) {
                LOG.severe("BAD BAD BAD :-( content = null");
                return null;
            }
            
            String anonToolUrl = "http://privacynet:8080/processor/run";
            LOG.info("Making request to ");
            try {
                content = new String(Base64.getDecoder().decode(content.getBytes()), StandardCharsets.UTF_8);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                ArrayList<MediaType> accepted = new ArrayList<>();
                accepted.add(MediaType.APPLICATION_JSON);
                headers.setAccept(accepted);

                Gson gson = new Gson();
                PrivacyNetRequest privnetReq = new PrivacyNetRequest();
                privnetReq.input = content;
                privnetReq.rules.add("csv");
                privnetReq.rules.add("ipmask.sa(255.255.0.0)");
                privnetReq.rules.add("ipmask.da(255.255.0.0)");
                privnetReq.rules.add("csvout");
                privnetReq.debug = true;

                //HttpEntity<String> req = new HttpEntity<>("{\"input\": \""+plainContent+"\", "
                // + "\"rules\": [\"csv\",\"ipmask.sa(255.255.0.0)\",\"ipmask.sa(255.255.0.0)\",\"csvout\"], \"debug\":true\n}", headers);
                String payload = gson.toJson(privnetReq);
                LOG.info("Payload: " + payload);
                HttpEntity<String> req = new HttpEntity<>(payload, headers);

                ResponseEntity<String> resp = restTemplate.postForEntity(anonToolUrl, req, String.class);
                LOG.info("PrivacyNet response. " + resp.getBody());
                PrivacyNetResponse response = gson.fromJson(resp.getBody(), PrivacyNetResponse.class);

                content = encodeBase64(response.output.getBytes());
            } catch(RestClientException e) {
                LOG.severe("Error in privacynet call: " + e.getMessage());
                e.printStackTrace();
            }
            
            return content;
        }
        
        public String runAnonymizationSparta(DataManipulationObject dmo, String param) {
            String content = dmo.getAdditionalInfo().get("content");
            if (content == null) {
                LOG.severe("BAD BAD BAD :-( content = null");
                return null;
            }
            
            String anonToolUrl = "http://anonymizator:5000/sparta?case="+param;
            LOG.info("Making request to " + anonToolUrl);
            try {
                content = decodeBase64(content);
                File tmpFile = File.createTempFile("tmp-c3isp", ".tmp");
                tmpFile.deleteOnExit();
                
                FileUtils.writeStringToFile(tmpFile, content, Charset.defaultCharset());

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                MultiValueMap<String, Object> body  = new LinkedMultiValueMap<>();
                body.add("dmofile", new FileSystemResource(tmpFile));

                HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
                ResponseEntity<String> resp = restTemplate.postForEntity(anonToolUrl, requestEntity, String.class);
                LOG.info("anonymizator response: " + resp.getBody());
                
                content = encodeBase64(resp.getBody().getBytes());
            } catch(RestClientException e) {
                LOG.severe("Error in anonymizator call: " + e.getMessage());
                e.printStackTrace();
            }
            catch (IOException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
            
            return content;
        }
        
        public static String encodeBase64(byte[] bytesData) {
            return new String(Base64.getEncoder().encode(bytesData));
        }
        
	public static String decodeBase64(String b64data) {
            return new String(Base64.getDecoder().decode(b64data));
	}

}
