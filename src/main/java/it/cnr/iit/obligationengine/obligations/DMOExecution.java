package it.cnr.iit.obligationengine.obligations;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.logging.Logger;

import it.cnr.iit.common.types.DataManipulationObject;

public class DMOExecution {

        
	private static final Logger log = Logger.getLogger(DMOExecution.class.getName());

	public static String executeDMO(DataManipulationObject dmo) {
                log.info("Execute dmo: " + dmo.getName());
                
		switch (dmo.getName()) {
		case "AnonymiseIPv4ByRandomization":
			return anonymiseIPv4ByRandomization(dmo);
		case "NotifyUserOn":
			new NotifyUserOn().notifyUserOn(dmo);
			break;
		default:
			log.severe("unknown dmo to execute");
		}
		return null;
	}

	private static String anonymiseIPv4ByRandomization(DataManipulationObject dmo) {
		log.severe("performing IPv4 anonymization by randomization...");
		String content = dmo.getAdditionalInfo().get("content");
                if (content == null) {
                    log.severe("BAD BAD BAD :-( content = null");
                    return null;
                }
		String IPV4_PATTERN = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
		content = new String(Base64.getDecoder().decode(content.getBytes()), StandardCharsets.UTF_8);
		content = content.replaceAll(IPV4_PATTERN, "*\\.*\\.*\\.*");
		content = new String(Base64.getEncoder().encode(content.getBytes()), StandardCharsets.UTF_8);
		return content;
	}

}
