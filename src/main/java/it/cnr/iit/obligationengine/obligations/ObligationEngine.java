package it.cnr.iit.obligationengine.obligations;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import org.w3c.dom.Node;

import it.cnr.iit.common.types.DataManipulationObject;

public final class ObligationEngine {

	private static final Logger log = Logger.getLogger(ObligationEngine.class.getName());

	public static DataManipulationObject processObligation(Node element) {
		try {
			Node xacmlRuleNode = element.getParentNode().getParentNode();
			log.severe("in processObligation target found: "
					+ xacmlRuleNode.getAttributes().getNamedItem("RuleId").getNodeValue());
			String val = element.getAttributes().getNamedItem("ObligationId").getNodeValue();
			String val_decoded = URLDecoder.decode(val, StandardCharsets.UTF_8.toString()).toString();
			String actionID = val.split("%")[0];
                        String attribute = null;

			log.severe("detected actionID: " + actionID);

                        if (val_decoded.contains("{") && val_decoded.contains("}"))
                        {
                            attribute = val_decoded.split("\\{")[1].split("\\}")[0];
                            log.severe("detected attribute: " + attribute);
                            return splitAttribute(actionID, attribute);
                        }
                        
                        return new DataManipulationObject(actionID);
                        
//			return performObligation(actionID, attribute);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//	private static DataManipulationObject performObligation(String action, String attributes) {
//		switch (action) {
//		case ObligationConstants.NOTIFYUSERON:
//			new NotifyUserOn().notifyUserOn(attributes);
//			return null;
//		default:
//			log.severe("unrecognized obligation to perform, considering it a DMO");
//			return splitAttribute(action, attributes);
//		}
//	}

	private static DataManipulationObject splitAttribute(String action, String attributes) {
		String[] arr = attributes.split(" ");

		String[] arrParam = arr[0].split("=");
		String[] arrOpt = arr[1].split("=");

                if (arrParam.length < 2 || arrOpt.length < 2)
                    return new DataManipulationObject(action);
                
		return new DataManipulationObject(action, arrParam[1], arrOpt[1]);
	}

}
